AntiFeatures:NonFreeDep
Categories:Office
License:MIT
Web Site:https://github.com/afollestad/cabinet/blob/HEAD/README.md
Source Code:https://github.com/afollestad/cabinet
Issue Tracker:https://github.com/afollestad/cabinet/issues

Auto Name:Cabinet Beta
Summary:File manager
Description:
Minimal file manager designed for Android 4.1 and above. However, updates
might be slow or might not come at all, since the author has no longer the
time to maintain it. Pullrequests are welcome.
.

Repo Type:git
Repo:https://github.com/afollestad/cabinet

Build:1.8.1,71
    commit=a6440d5bd4dd95a8416a86305527a6f86aa49020
    subdir=app
    gradle=yes
    rm=app/libs/*
    prebuild=sed -i -e '/fileTree/acompile "com.jcraft:jsch:0.1.51"\ncompile "com.anjlab.android.iab.v3:library:1.0.8@aar"' build.gradle

Build:1.8.2,74
    commit=6f40d75491728fe45bd68bb1c1c08100e1bd9dfa
    subdir=app
    gradle=yes
    rm=app/libs/*
    prebuild=sed -i -e '/fileTree/acompile "com.jcraft:jsch:0.1.51"\ncompile "com.anjlab.android.iab.v3:library:1.0.8@aar"' build.gradle

Build:1.8.3,75
    commit=4ab08e6acc6bc6dfb84365fb604e92382bd2df6a
    subdir=app
    gradle=yes
    rm=app/libs/*
    prebuild=sed -i -e '/fileTree/acompile "com.jcraft:jsch:0.1.51"\ncompile "com.anjlab.android.iab.v3:library:1.0.8@aar"' build.gradle

Build:1.8.3.1,78
    commit=ca6ece5f54ca8892a0415d4db41f05e22e469e3a
    subdir=app
    gradle=yes
    rm=app/libs/*
    prebuild=sed -i -e '/fileTree/acompile "com.jcraft:jsch:0.1.51"\ncompile "com.anjlab.android.iab.v3:library:1.0.8@aar"' build.gradle

Build:1.8.3.3,82
    commit=b42d76b7548e11dcb2040547f27ca3571afb1cef
    subdir=app
    gradle=yes
    rm=app/libs/*

Build:1.8.3.4,83
    disable=submodule not yet published
    commit=00ec947bed7aa3a97a883b119a8efa401a60ba73
    subdir=app
    submodules=yes
    gradle=yes
    rm=app/libs/*

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.8.4
Current Version Code:86

